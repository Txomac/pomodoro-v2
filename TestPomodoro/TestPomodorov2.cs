﻿using NUnit.Framework.Legacy;
using pomodoro.Models;
using System.Threading;

namespace PomodoroTests
{
    public class PomodoroTest
    {
        private Pomodorov2 _pomodoro;

        [SetUp]
        public void Setup()
        {
            _pomodoro = new Pomodorov2(25, 5);
        }

        [Test]
        public void TestInitialValues()
        {
            ClassicAssert.IsTrue(_pomodoro.GetIsWorkSession());
            ClassicAssert.AreEqual(25 * 60, _pomodoro.GetRemainingSeconds());
        }

        [Test]
        public void TestSwitchTimer()
        {
            _pomodoro.SwitchTimer();
            ClassicAssert.IsFalse(_pomodoro.GetIsWorkSession());
            ClassicAssert.AreEqual(5 * 60, _pomodoro.GetRemainingSeconds());

            _pomodoro.SwitchTimer();
            ClassicAssert.IsTrue(_pomodoro.GetIsWorkSession());
            ClassicAssert.AreEqual(25 * 60, _pomodoro.GetRemainingSeconds());
        }

        [Test]
        public void TestStartStopTimer()
        {
            bool stateChanged = false;
            _pomodoro.OnStateChanged += () => stateChanged = true;

            _pomodoro.Start();
            Thread.Sleep(1100);  // Sleep for a bit more than a second to allow the timer to tick
            _pomodoro.Stop();

            ClassicAssert.Less(_pomodoro.GetRemainingSeconds(), 25 * 60);
            ClassicAssert.IsFalse(stateChanged);  // State should not have changed yet
        }

        [Test]
        public void TestTimerElapsed()
        {
            int elapsedTime = 0;
            _pomodoro.OnTimeChanged += (remainingTime) => elapsedTime = remainingTime;

            _pomodoro.Start();
            Thread.Sleep(1100);  // Sleep for a bit more than a second to allow the timer to tick
            _pomodoro.Stop();

            ClassicAssert.AreEqual(_pomodoro.GetRemainingSeconds(), elapsedTime);
            ClassicAssert.Less(_pomodoro.GetRemainingSeconds() , 25 * 60);
        }

        [Test]
        public void TestReset()
        {
            _pomodoro.Start();
            Thread.Sleep(1100);  // Sleep for a bit more than a second to allow the timer to tick
            _pomodoro.Stop();
            _pomodoro.Reset();

            ClassicAssert.AreEqual(25 * 60, _pomodoro.GetRemainingSeconds());
        }

        [Test]
        public void TestSetWorkAndBreakMinutes()
        {
            _pomodoro.SetWorkMinutes(30);
            _pomodoro.SetBreakMinutes(10);

            _pomodoro.Reset();
            ClassicAssert.AreEqual(30 * 60, _pomodoro.GetRemainingSeconds());
            ClassicAssert.AreEqual(30, _pomodoro.GetWorkMinutes());
            ClassicAssert.AreEqual(10, _pomodoro.GetBreakMinutes());

            _pomodoro.SwitchTimer();
            ClassicAssert.AreEqual(10 * 60, _pomodoro.GetRemainingSeconds());
        }
    }
}
