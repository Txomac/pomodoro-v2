﻿using System;
using System.Timers;

namespace pomodoro.Models
{
    public class Pomodorov2
    {
        public event Action? OnStateChanged;
        public event Action<int>? OnTimeChanged;

        private System.Timers.Timer _timer;
        private int _workMinutes;
        private int _breakMinutes;
        private bool _isWorkSession;
        private int _remainingTime;

        public Pomodorov2(int workMinutes, int breakMinutes)
        {
            _workMinutes = workMinutes;
            _breakMinutes = breakMinutes;
            _isWorkSession = true;
            _remainingTime = _workMinutes * 60;
            _timer = new System.Timers.Timer(1000);
            _timer.Elapsed += Timer_Elapsed;
        }

        public bool IsWorkSession
        {
            get { return _isWorkSession; }
            set { _isWorkSession = value; }
        }

        public int RemainingSeconds
        {
            get { return _remainingTime; }
            set { _remainingTime = value; }
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void SwitchTimer()
        {
            if (_isWorkSession)
            {
                _remainingTime = _breakMinutes * 60;
                _isWorkSession = false;
            }
            else
            {
                _remainingTime = _workMinutes * 60;
                _isWorkSession = true;
            }

            OnStateChanged?.Invoke();
        }

        public int GetRemainingSeconds()
        {
            return _remainingTime;
        }

        public bool GetIsWorkSession()
        {
            return _isWorkSession;
        }

        public void SetWorkMinutes(int workMinutes)
        {
            _workMinutes = workMinutes;
        }

        public void SetBreakMinutes(int breakMinutes)
        {
            _breakMinutes = breakMinutes;
        }
        public int GetWorkMinutes()
        {
            return _workMinutes;
        }

        public int GetBreakMinutes()
        {
            return _breakMinutes;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _remainingTime--;

            if (_remainingTime <= 0)
            {
                SwitchTimer();
            }

            OnTimeChanged?.Invoke(_remainingTime);
        }

        public void Reset()
        {
            _remainingTime = _isWorkSession ? _workMinutes * 60 : _breakMinutes * 60;
        }

    }
}
